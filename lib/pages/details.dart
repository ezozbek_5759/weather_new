import 'package:flutter/material.dart';

class Details extends StatefulWidget {
  const Details({super.key});

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AspectRatio'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 40,
            ),
            Container(
              height: 200,
              width: double.infinity,
              color: Colors.lightBlue,
              alignment: Alignment.center,
              child: AspectRatio(
                aspectRatio: 3 / 4,
                child: Container(
                  color: Colors.orange,
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              height: 200,
              width: 200,
              color: Colors.brown,
              alignment: Alignment.center,
              child: AspectRatio(
                aspectRatio: 2.0,
                child: Container(
                  // width: 100,
                  // height: 50,
                  alignment: Alignment.center,
                  color: Colors.amberAccent,
                  child: Text(
                    'Salom',
                    style: TextStyle(fontSize: 24),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              height: 200,
              width: 200,
              color: Colors.brown,
              alignment: Alignment.center,
              child: AspectRatio(
                aspectRatio: 0.5,
                child: Container(
                  // width: 100,
                  // height: 50,
                  color: Colors.amberAccent,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
